const gulp = require("gulp");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass");

//style paths
let sassFiles = "sass/*.scss";
let cssDest = "css";

gulp.task("sass", function () {
  return gulp
    .src(sassFiles)
    .pipe(sass().on("error", sass.logError)) // Using gulp-sass
    .pipe(gulp.dest(cssDest))
    .pipe(browserSync.stream());
});

gulp.task("serve", function () {
  browserSync.init({
    notify: false,
    server: ".",
  });
  gulp.watch("*.html", browserSync.reload);
  gulp.watch("js/*.js", browserSync.reload);
  gulp.watch("css/*.css", browserSync.reload);
  gulp.watch(sassFiles, gulp.series("sass"), browserSync.reload);
});
